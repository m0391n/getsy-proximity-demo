package com.m039.getsyproximity.demo.app;

import android.app.Application;
import android.content.res.Resources;
import com.m039.beacon.keeper.service.BeaconService;
import com.m039.getsyproximity.demo.R;
import com.m039.getsyproximity.library.GetsyProximity;

/**
 * Created by GTO on 09.08.2014.
 */
public class DemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Resources res = getResources();

        String appId = res.getString(R.string.getsy_proximity__app_id);
        String appToken = res.getString(R.string.getsy_proximity__app_token);

        GetsyProximity gp = GetsyProximity.getInstance(this);

        gp.setDebug(true);
        gp.useDevelopServer();
        gp.initializeSDK(appId, appToken);

        BeaconService.onApplicationCreate(this);
    }
    
}
