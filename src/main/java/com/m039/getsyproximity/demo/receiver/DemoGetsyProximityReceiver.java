package com.m039.getsyproximity.demo.receiver;

import android.content.Context;
import com.m039.getsyproximity.demo.activity.DemoNotificationActivity;
import com.m039.getsyproximity.demo.R;
import com.m039.getsyproximity.library.model.BeaconData;
import com.m039.getsyproximity.library.model.response.GetExperiencesResponse;
import com.m039.getsyproximity.library.GetsyProximity;
import com.m039.getsyproximity.library.receiver.GetsyProximityReceiver;

/**
 * Created by GTO on 24.08.2014.
 */
public class DemoGetsyProximityReceiver extends GetsyProximityReceiver {

    @Override
    public void onGetExperience
        (
         Context context,
         GetExperiencesResponse.Experience experience,
         BeaconData beaconData
         )
    {
        GetsyProximity
            .getInstance(context)
            .pushNotification(DemoNotificationActivity.class,
                              experience,
                              context.getString(R.string.app_name),
                              R.drawable.ic_launcher);
    }
}
