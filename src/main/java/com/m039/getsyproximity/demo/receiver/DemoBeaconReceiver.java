package com.m039.getsyproximity.demo.receiver;

import android.content.Context;
import android.content.Intent;
import com.m039.beacon.keeper.content.BeaconEntity;
import com.m039.beacon.keeper.service.BeaconService;
import com.m039.getsyproximity.library.GetsyProximity;

/**
 * Created by GTO on 24.08.2014.
 */
public class DemoBeaconReceiver extends com.m039.beacon.keeper.receiver.BeaconReceiver {

    private static final String TAG = "DemoBeaconReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            BeaconService.onBootCompleted(context);
            android.util.Log.d(TAG, "onBootCompleted");
        } else {
            super.onReceive(context, intent);
        }
    }

    @Override
    protected void onFoundBeacon(Context context, BeaconEntity beaconEntity) {
        android.util.Log.d(TAG, "onFoundBeacon | " + beaconEntity.getIBeacon());

        GetsyProximity
            .getInstance(context)
            .onFoundBeacon(beaconEntity.getUuid(),
                           beaconEntity.getMajor(),
                           beaconEntity.getMinor(),
                           beaconEntity.getRssi(),
                           beaconEntity.getAccuracy());
    }
}
