package com.m039.getsyproximity.demo.activity;

import android.app.Activity;
import android.os.Bundle;
import com.m039.getsyproximity.demo.R;
import com.m039.getsyproximity.library.GetsyProximity;

/**
 * Created by GTO on 09.08.2014.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);

        GetsyProximity
            .getInstance(this)
            .userLogin(getString(R.string.getsy_proximity__user_mail), GetsyProximity.LOGIN_TYPE_EMAIL);
    }

}
