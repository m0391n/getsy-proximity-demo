package com.m039.getsyproximity.demo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.m039.getsyproximity.demo.R;
import com.m039.getsyproximity.library.model.response.GetExperiencesResponse;
import com.m039.getsyproximity.library.activity.GetsyNotificationActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by GTO on 12.08.2014.
 */
public class DemoNotificationActivity extends GetsyNotificationActivity
    implements View.OnClickListener 
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_demo_notification);
    }

    @Override
    protected void onResume () {
        super.onResume ();

        GetExperiencesResponse.Experience.Content content = getExperience().getContent();

        TextView contentText = (TextView) findViewById(R.id.content_text);
        TextView contentTitle = (TextView) findViewById(R.id.content_title);
        ImageView contentImage = (ImageView) findViewById(R.id.content_image);
        View close = findViewById(R.id.close);

        contentText.setText(content.getText());
        contentTitle.setText(content.getTitle());

        //
        // NOTE: Colors doesn't work yet
        // 
        // contentText.setTextColor(content.getTextColor());
        // contentTitle.setTextColor(content.getTitleColor());
        // findViewById(android.R.id.content).setBackgroundColor(content.getBgColor());
        //

        Picasso.with(this)
            .load(content.getImg())
            .centerCrop()
            .resizeDimen(R.dimen.a_demo_notification__image_width, R.dimen.a_demo_notification__image_height)
            .into(contentImage);

        close.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close :
                this.finish();
                break;
        }
    }
}
